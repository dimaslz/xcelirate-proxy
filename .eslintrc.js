module.exports = {
    env: {
        es2020 : true,
        node   : true,
    },
    extends: [
        // 'standard'
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier',
        'prettier/@typescript-eslint',
        'plugin:import/errors',
        'plugin:import/warnings'
    ],
    parser        : '@typescript-eslint/parser',
    parserOptions : {
        ecmaVersion : 11,
        sourceType  : 'module',
    },
    plugins: [
        '@typescript-eslint/eslint-plugin',
        'unused-imports'
    ],
    rules: {
        '@typescript-eslint/no-inferrable-types': [
            'warn', {
                ignoreParameters: true,
            }
        ],
        '@typescript-eslint/no-unused-vars' : 'warn',
        'import/no-extraneous-dependencies' : 'off',
        'linebreak-style'                   : 'off',
        'no-tabs'                           : 'off',
        'object-curly-spacing'              : ['error', 'always'],
        semi                                : ['error', 'always'],
        'max-len'                           : [
            'error',
            {
                comments : 180,
                code     : 180,
            }
        ],
        '@typescript-eslint/indent': [
            'error',
            4,
            {
                SwitchCase         : 1,
                VariableDeclarator : 1,
                outerIIFEBody      : 1,
            }
        ],
        '@typescript-eslint/interface-name-prefix'          : 'off',
        '@typescript-eslint/explicit-function-return-type'  : 'off',
        '@typescript-eslint/no-explicit-any'                : 'off',
        '@typescript-eslint/explicit-module-boundary-types' : 'off',
        'comma-dangle'                                      : ['error', { objects: 'always', }],
        'key-spacing'                                       :
      [
          'error',
          {
              align: {
                  beforeColon : true,
                  afterColon  : true,
                  on          : 'colon',
              },
          }
      ],
        'import/no-unresolved': [
            2,
            { caseSensitive: false, ignore: ['.'], }
        ],
        'import/order'                     : ['error', { alphabetize: { order: 'asc', caseInsensitive: true, }, }],
        'import/no-unused-modules'         : [1, { unusedExports: true, }],
        'comma-spacing'                    : ['error', { before: false, after: true, }],
        'no-unused-vars'                   : 'off',
        'unused-imports/no-unused-imports' : 'error',
        'function-call-argument-newline'   : ['error', 'consistent'],
        'lines-between-class-members'      : ['error', 'always'],
        'no-multiple-empty-lines'          : ['error', { max: 2, maxEOF: 0, }],
    },
};