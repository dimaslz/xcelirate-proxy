import errorHandler from "errorhandler";
import moduleAlias from "module-alias";
moduleAlias.addAlias('@', __dirname + '/');

import 'module-alias/register';


import { default as APP } from "./app";

const { app, http, } = APP;
/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
const server = http.listen(app.get("port"), () => {
    console.log(
        "  App is running at http://localhost:%d in %s mode",
        app.get("port"),
        app.get("env")
    );
    console.log("  Press CTRL-C to stop\n");
});

export default server;
