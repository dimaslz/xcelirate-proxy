import axios from "axios";
import express, { Response, Request } from "express";

const app = express();
const router = express.Router();

router.post("/track", async (httpRequest: Request, httpResponse: Response) => {
    const { body, } = httpRequest;

    const { data, }: any = await axios({
        url    : 'https://postman-echo.com/post',
        method : 'POST',
        data   : body,
    });

    return httpResponse.status(200).json(data);
});

app.use(router);

export default app;