import fs from "fs";
import dotenv from "dotenv";
import logger from "./logger";

if (fs.existsSync(".env")) {
    logger.debug("Using .env file to supply config environment variables");
    dotenv.config({ path: ".env", });
} else {
    logger.debug("Using .env.example file to supply config environment variables");
    dotenv.config({ path: ".env.example", });  // you can delete this after you create your own .env file!
}
export const {
    PORT,
    SESSION_SECRET,

} = process.env;

if (!SESSION_SECRET) {
    logger.error("No client secret. Set SESSION_SECRET environment variable.");
    process.exit(1);
}
