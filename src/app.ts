/* eslint-disable @typescript-eslint/no-var-requires */
import _http from "http";
import path from "path";
import bodyParser from "body-parser";
import compression from "compression";  // compresses requests
import cors from "cors";
import express from "express";
import session from "express-session";
import lusca from "lusca";

// Controllers (route handlers)
import trackController from "./controllers/track.controller";

import {
    SESSION_SECRET,
    PORT
} from "./util/secrets";


// Create Express server
const app = express();
const http = new _http.Server(app);

const corsOptions = {
    origin               : "*",
    // allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With', 'Accept'],
    methods              : ["GET", "PUT", "POST", "DELETE", "OPTIONS", "PATCH", "HEAD"],
    optionsSuccessStatus : 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));

// Connect to MongoDB
if (process.env.NODE_ENV !== "test") {
    app.use(session({
        resave            : true,
        saveUninitialized : true,
        secret            : SESSION_SECRET,
    }));
}

// Express configuration
app.set("port", PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, }));
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000, })
);

/**
 * Primary app routes.
 */
app.use("/", trackController);

export default {
    app,
    http,
};