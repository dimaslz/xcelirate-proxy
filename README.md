# Xcelirate (API)
Xcelirate Proxy api

## Stack
* Express
* TypeScript

## Scripts
| Npm Script | Description |
| ------------------------- | ------------------------------------------------------------------------------------------------- |
| `build`                   | Full build. Runs ALL build tasks (`build-sass`, `build-ts`, `lint`, `copy-static-assets`)         |
| `build-ts`                | Compiles all source `.ts` files to `.js` files in the `dist` folder                               |
| `debug`                   | Performs a full build and then serves the app in watch mode                                       |
| `lint`                    | Runs ESLint on project files                                                                      |
| `serve`                   | Runs node on `dist/server.js` which is the apps entry point                                       |
| `serve-debug`             | Runs the app with the --inspect flag                                                              |
| `start`                   | Does the same as 'npm run serve'. Can be invoked with `npm start`                                 |
| `test`                    | Runs tests using Jest test runner                                                                 |
| `watch`                   | Runs all watch tasks (TypeScript, Sass, Node). Use this if you're not touching static assets.     |
| `watch-debug`             | The same as `watch` but includes the --inspect flag so you can attach a debugger                  |
| `watch-node`              | Runs node with nodemon so the process restarts if it crashes. Used in the main watch task         |
| `watch-test`              | Runs tests in watch mode                                                                          |
| `watch-ts`                | Same as `build-ts` but continuously watches `.ts` files and re-compiles when needed               |

## Author
Dimas López · FullStack Software development

🐦  [https://twitter.com/dimaslz](https://twitter.com/dimaslz)

👨🏻‍💻  [https://dimaslz.dev](https://dimaslz.dev)

📄  [https://www.linkedin.com/in/dimaslopezzurita](https://www.linkedin.com/in/dimaslopezzurita)